
/**
 * Décrivez votre classe Fraction ici.
 *
 * @author (ILBOUDO)
 * @version (1)
 */
public class Fraction
{
    private int numerateur;
    private int denominateur;
    
    public Fraction(int numerateur, int denominateur){
        this.numerateur = numerateur;
        this.denominateur = denominateur;
    }
    
    public Fraction(){
        this.numerateur = 0;
        this.denominateur = 1;
    }
    

    public int getNumerateur(){
        return numerateur;
    }

    public int getDenominateur(){
        return denominateur;
    }

    public void setNumerateur(int numerateur){
        this.numerateur = numerateur;
    }

    public void setDenominateur(int denominateur){
        this.denominateur = denominateur;
    }

    public double consultValeurDouble (){
        return (double) numerateur/denominateur;
        //System.out.println(numerateur+"/"+denominateur);
    }


    public Fraction additionner(Fraction f1,Fraction f2){

        Fraction f = new Fraction(1,1);
        f.setNumerateur((f1.numerateur*f2.denominateur)+(f2.numerateur*f1.denominateur));
        f.setDenominateur((f1.denominateur*f2.denominateur));
        return f;

    }

    
    public String toString(){
        return numerateur+"/"+denominateur;
    }
    
    public void comparaison(Fraction f1, Fraction f2){
        if(((double) f1.numerateur/f1.denominateur)==((double) f2.numerateur/f2.denominateur))
            {System.out.println("Les 2 fractions sont équales");}
        else{
            if(((double) f1.numerateur/f1.denominateur)<((double) f2.numerateur/f2.denominateur)){
                System.out.println("La fraction 2 est plus grande");
            }
            else {
                System.out.println("La fraction 1 est plus grande");
            }
        }
    }

}
